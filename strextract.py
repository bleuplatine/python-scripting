#!/usr/bin/env python
import os
import re
import argparse


# fonction de recherche/extraction des chaines littérales
def browse_extract(directory, suffix, path, hidden):
    # effectuer un controle d'existence de répertoire
    if not os.path.isdir(directory):
        print(f"{directory} does not exist")
        exit()
    # effectuer une recherche fichier par fichier dans le répertoire saisi
    for rootfolder, _, filenames in os.walk(directory):
        for filename in filenames:
            # --suffixe : afficher ou non les résultats comprenant le suffixe saisi
            if suffix and not filename.endswith(suffix):
                continue
            # --all : afficher ou non les résultats comprenant les fichiers cachés
            if hidden and filename.startswith('.'):
                continue
            # ouvrir chaque fichier et le parcourir ligne par ligne
            filepath = os.path.join(rootfolder, filename)
            with open(filepath, 'r') as file:
                for line in file:
                    # trouver chaque chaîne littérale ("ou') située dans chaque ligne
                    for value in re.findall(r"['](.*?)[']|[\"](.*?)[\"]", line):
                        # --path : afficher les chaines précédées de leur chemin 
                        if path:
                            if value[0]:
                                print(filepath,
                                  f"'{value[0]}'", sep='\t')
                            else:
                                print(filepath,
                                  f'"{value[1]}"', sep='\t')
                        # afficher les chaines seules
                        else:
                            if value[0]:
                                print(f"'{value[0]}'")
                            else:
                                print(f'"{value[1]}"')

def main():
    my_parser = argparse.ArgumentParser(prog='strextract',
                                        description="browse all files in a directory hierarchy and extract all literal strings")

    # argument obligatoire > chemin du répertoire
    my_parser.add_argument('directory',
                        help='path to the directory')

    # liste des arguments optionnels
    my_parser.add_argument('--suffix', action='store',
                        help='limit search to a particular suffix files')
    my_parser.add_argument('--path', action='store_true',
                        help='precede each result with its associated file path')
    my_parser.add_argument('-a', '--all', action='store_false',
                        help="include hidden files (begin with '.')")
    args = my_parser.parse_args()

    # liste des var argparse > print(vars(args))
    directory = args.directory
    suffix = args.suffix
    path = args.path
    hidden = args.all
    browse_extract(directory, suffix, path, hidden)


if __name__ == '__main__':
    main()


