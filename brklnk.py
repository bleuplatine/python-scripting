#!/usr/bin/env python
import requests
import argparse
from bs4 import BeautifulSoup
import urllib

# fonction récursive de recherche/extraction de liens cassés 
def broken_links(url, depth):
    # tant que la profondeur maxi n'est pas atteinte
    if depth >= 0:
        # récupérer le protocole http du lien saisi
        response = requests.get(url)
        # vérifier que le lien est cassé (retourne code 404)
        if response.status_code >= 400:
            print(f"{url}")
            return
        # vérifier que le lien est valide
        if not response:
            print(f"{url} does not exist")          
            return
        # récupérer le code html de la page concernée
        soup = BeautifulSoup(response.text, "html.parser")
        # trouver chaque lien situé dans le code html et lui appliquer la fonction de recherche/extraction de liens cassés
        for link in soup.find_all('a'):
            # créer une url absolue par jonction de l'url de base et du lien relatif
            link_url = urllib.parse.urljoin(url, link.get('href'))
            broken_links(link_url, depth - 1)
        
def main():
    my_parser = argparse.ArgumentParser(prog='brklnk',
                                        description="browse all the links contained in a web page and displays the broken links")
    # argument obligatoire > url du site web à scanner
    my_parser.add_argument('url',
                        help='the website url to scan')
    # profondeur de recherche
    my_parser.add_argument('--depth', action='store', nargs='?',
                        default=1, type=int,
                        help='the search depth (1 by default)')
    args = my_parser.parse_args()
    # liste des var argparse > print(vars(args))
    url = args.url
    depth = args.depth

    broken_links(url, depth)

if __name__ == '__main__':
    main()